#her gjør jeg noe først
"""
navn = input('navn: ')
alder = int(input('alder: '))
while len(navn) <= 4 or alder < 18:
    print('navn minst fem bokstaver, alder minst 18')
    navn = input('navn: ')
    alder = int(input('alder: '))

# her gjør jeg noe mer

"""
# men denne logikken kan en heller sette ut til en funksjon.
def get_proper_name_and_age():
    while True:
        print('navn minst fem bokstaver, alder minst 18')
        navn = input('navn: ')
        if navn == 'børge':
            break # Hvis Børge, gå rett ut av løkka!
        if navn == 'runar':
            continue # Hvis runar, gå til start av løkka
        alder = int(input('alder: '))
        if alder > 17 and len(navn) > 4:
            print('yaya')
            return navn, alder # Dette er vanlig utgang av funksjonen
    print('her') # Se når denne skrives ut, vi kommer bare hit når navn er børge
    return('dummy', 18)

navn, alder = get_proper_name_and_age()
print(navn, alder)

